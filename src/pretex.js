
/// A preprocessor extension Asciidoctor.js that converts TeX and LaTeX inline and display
/// math expressions to their equivalent Asciidoc latexmath:[...] and [latexmath] forms.
///
/// There is also a tiny Asciidoctor register function at the end of the file that links
/// the preprocessor in as an asciidoctor extension.
///
/// @copyright Copyright (c) 2023 Nessan Fitzmaurice
"use strict";

function Pretex() {
  const self = this;
  self.process(function (document, reader) {
    if (reader.isEmpty()) return;

    //const regex = /(?<![\\$"'`])\$(?!`)(.*?)(?<![$\\])\$/gm;

    // Match on TeX inline math expressions.
    // See https://regex101.com/r/U2QVdV/2
    const tex_rx = /(?<![\\+`])(?<!\$)\$(?![$`])(.*?)(?<![\\$])\$(?![$`+])/gm;

    // Match on LaTeX inline math expressions.
    // See https://regex101.com/r/MI2M3d/1
    const latex_rx = /(?<![\\`+])\\\((.*?)\\\)(?![`+])/gm;

    // In both cases we replace the inline math with this substitution
    const subst = "latexmath:[$1]";

    // Where to put the converted text
    const replacement_lines = [];

    // Do nothing with the input until we see a :pretex: directive
    let do_substitutions = false;

    // Process the lines
    while (!reader.isEmpty()) {
      // Current line we are processing
      const line = reader.readLine();

      // Toggle the `do_substitutions` flag if we see :pretex:
      if (line == ":pretex:") {
        do_substitutions = !do_substitutions;
        continue;
      }

      // Maybe need to do nothing at all?
      if (!do_substitutions) {
        replacement_lines.push(line);
        continue;
      }

      // Display math?
      const look_for = line == "$$" ? "$$" : line == "[" ? "]" : "";
      if (look_for != "") {
        // Replace the display-math begin line with the two appropriate Asciidoc lines
        replacement_lines.push("[latexmath]");
        replacement_lines.push("++++");

        // Look for the display-math end line that closes out this block
        let in_display_math = true;
        while (!reader.isEmpty()) {
          const line = reader.readLine();
          if (line === look_for) {
            // Found it! Replace with the Asciidoc end of passthrough line.
            replacement_lines.push("++++");
            in_display_math = false;
            break;
          } else {
            // No luck -- the line must be part of the display-math block ...
            replacement_lines.push(line);
          }
        }

        // Raise an error if we get to here and did not find a closing '$$'
        if (in_display_math) {
          console.warn("pretex: Unterminated display-math block!");
        }

        // Move on to the next line
        continue;
      }

      //  Possibility of inline math so run the line through our TeX & LaTeX regex's
      replacement_lines.push(line.replace(tex_rx, subst).replace(latex_rx, subst));
    }

    // Set the reader up with all the preprocessed lines
    reader.$unshift_lines(replacement_lines);
  });
}

module.exports.register = function register(registry) {
  registry.preprocessor(Pretex);
  return registry;
};
