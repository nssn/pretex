# A preprocessor extension for Asciidoctor that converts TeX and LaTeX inline and display
# math expressions to their equivalent Asciidoc latexmath:[...] and [latexmath] forms.
#
# There is also a tiny Asciidoctor.Extensions.register function at the end of the
# file that links in the preprocessor as an asciidoctor extension.
#
# USAGE:
# `asciidoctor -r ./pretex.rb` sample.adoc should create sample.html.
#
# @copyright Copyright (c) 2023 Nessan Fitzmaurice
require 'asciidoctor/extensions' unless RUBY_ENGINE == 'opal'

include Asciidoctor
include Logging

class Pretex < Asciidoctor::Extensions::Preprocessor

    # Regex to match TeX inline math $...$ and LaTeX inline math \(...\).
    # It will ignore things like $10, $$$'s and display math blocks $$\n ... $$\n'.
    # It will also ignore inline-math enclosed in backticks e.g.. `\(\TeX\)`.
    # Handles lots of cases but cetainly not all!
    REGEX =
    %r{
        # Match on TeX and LaTeX inline math expressions.
        # See https://regex101.com/r/B1NsRl/5
        # The math content will be in group 3.

        # Look behind--no matches starting with  \,`,or +.
        (?<![\\+`])

        # Look for the two possible inline-math begin strings.
        # The token(s) that form the begin are ultimately ignored.
        (?:
          # Group 1: A single $ sign OR
          ((?<!\$)\$(?!\$))           |
          # Group 2: An escaped left parenthesis.
          (\\\()
        )

        # Found an inline-math begin string so capture the content.
        # Group 3: Non-greedy capture of the inline-math content
        (.*?)

        # Stops capturing when we hit matching inline-math end string.
        # The token(s) that form the end are ultimately ignored.
        (?:
          # Group 1: End is  a non-escaped $.   ELSE
          (?(1)(?<![\\$])\$(?![$`+])             |
          # Group 2: End is an escaped right parenthesis.
          \\\)
        ))
    }x

    # In both cases we replace the inline math with this substitution
    # NOTE: The regex above puts the math content into group 3!
    SUBST = 'latexmath:[\3]'

    # Process all the lines in the document from the reader ...
    def process document, reader
        return reader if reader.eof?

        # Space to put our replacement lines in
        replacement_lines = []

        # We do nothing with the input until we see a :pretex: directive
        do_substitutions = false

        # Process the lines
        while !reader.empty?

            # Current line we are processing
            line = reader.read_line
            line_chomp = line.chomp

            # Toggle the `do_substitions` flag if we see the ':pretex:'
            if line_chomp == ':pretex:'
                do_substitutions = !do_substitutions
                next
            end

            # Maybe need to do nothing?
            if !do_substitutions
                replacement_lines << line
                next
            end

            # Display math?
            look_for = if line_chomp == '\[' then '\]' elsif line_chomp == '$$' then '$$' else '' end
            if !look_for.empty?
                # Replace the display-math begin line with the two appropriate Asciidoc lines
                replacement_lines << "[latexmath]" << "++++"

                # Look for the display-math end line that closes out this block
                in_display_math = true;
                while !reader.empty?
                    line = reader.read_line
                    if line.chomp == look_for
                        # Found it! Replace with the Asciidoc end of passthough line.
                        replacement_lines  << "++++"
                        in_display_math = false
                        break
                    else
                        # No luck -- the line must be part of the display-math block ...
                         replacement_lines << line
                    end
                end

                # Raise an error if we get here and there was no closing '$$'
                if in_display_math
                    logger.warn "pretex: Unterminated display-math block!"
                end

                # Move on to the next line
                next
            end

            # Possibility of inline math so run the line through our TeX/LaTeX regex
            replacement_lines << line.gsub(REGEX, SUBST)

        end

        # Set the reader up with all the preprocessed lines
        reader.unshift_lines replacement_lines
        reader
    end
end

# Register the extension
Extensions.register do
    preprocessor Pretex
end
